# BUILD
FROM node:14.15.3 as node

WORKDIR /app

COPY . .

RUN npm i 'file-saver@github:eligrey/FileSaver.js#1.3.8'
RUN npm install --force
RUN npm run build --prod

# DEPLOY

FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/conf.d/

# RUN ls /usr/share/nginx/html/

# RUN ls /usr/share/nginx/

# RUN rm -r /usr/share/nginx/html/*

COPY --from=node /app/dist /usr/share/nginx/html
