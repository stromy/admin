import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './materials.module';
import { ReactiveFormsModule } from '@angular/forms';

// COMPONENTS
import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { EditTreeComponent } from './components/edit-tree/edit-tree.component';
import { TreesTableComponent } from './components/trees-table/trees-table.component';
import { PrintTreeComponent } from './components/print-tree/print-tree.component';
import { EditPrimaryComponent } from './components/edit-tree/edit-primary/edit-primary.component';
import { EditClassificationComponent } from './components/edit-tree/classification/edit-classification.component';
import { EditBtnGroupComponent } from './components/edit-tree/classification/edit-btn-group/edit-btn-group.component';
import { EditDengerComponent } from './components/edit-tree/denger-tree/edit-denger.component';
import { EditDangerNodeComponent } from './components/edit-tree/denger-tree/edit-danger-node/edit-danger-tree.component';
import { EditGalleryComponent } from './components/edit-tree/edit-gallery/edit-gallery.component';
import { EditEditorComponent } from './components/edit-tree/edit-editor/edit-editor.component';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { DownloadsComponent } from './components/downloads/downloads.component';
import { LoginComponent } from './components/users/login/login.component';
import { RegisterComponent } from './components/users/register/register.component';
import { PasswordComponent } from './components/users/password/password.component';
import { MapComponent } from './components/map/map.component';

// SERVICES
import { EditTreeService } from './services/edit-tree.service';
import { LoadedTreesService } from './services/loaded-trees.service';
import { UserService } from './services/user.service';
import { AuthGuard } from './services/auth.guard';
import { AdminGuard } from './services/admin.guard';
import { FilterService } from './services/filter.service';
import { FilterComponent } from './components/trees-table/filter/filter.component';
import { FilterPaginatorComponent } from './components/trees-table/filter-paginator/filter-paginator.component';
import { FilterAreaComponent } from './components/trees-table/filter/filter-area/filter-area.component';
import { MapService } from './services/map.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
// import { TableDataService } from './components/trees-table/table-data.service';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HeaderComponent,
    EditTreeComponent,
    DownloadsComponent,
    TreesTableComponent,
    PrintTreeComponent,
    EditDengerComponent,
    EditDangerNodeComponent,
    EditEditorComponent,
    EditPrimaryComponent,
    EditGalleryComponent,
    ListUsersComponent,
    EditClassificationComponent,
    EditBtnGroupComponent,
    InfoDialogComponent,
    LoginComponent,
    RegisterComponent,
    PasswordComponent,
    FilterComponent,
    FilterPaginatorComponent,
    FilterAreaComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    EditTreeService,
    LoadedTreesService,
    UserService,
    AuthGuard,
    AdminGuard,
    FilterService,
    MapService
  ],
  bootstrap: [AppComponent],
  entryComponents: [InfoDialogComponent]
})
export class AppModule { }
