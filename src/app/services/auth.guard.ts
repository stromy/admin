import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UserService } from './user.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  canActivate(): boolean {
    // const currentUser = this.userService.currentUserRole;
    const currentUser = localStorage.getItem('role');
    this.userService.userSubject.next(currentUser as any);
    if (currentUser && currentUser !== '') {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}