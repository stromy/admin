import { Injectable } from '@angular/core';
import { Filter, Page, Kraj, Okres, Zchu, Orp  } from '../interfaces/filter.interface';
import { BehaviorSubject } from 'rxjs';
import { LoadedTreesService } from './loaded-trees.service';

@Injectable()
export class FilterService {
  filter: Filter = {
    validated: false, vlast: [], type: [],
    start: 0, n: 25,
    dateStart: '', dateEnd: '',
    kraj: [], okres: [], orp: [], mzchu: [], vzchu: [],
    center: [0, 0], radius: 0,
  };

  filterSubject: BehaviorSubject<Filter> = new BehaviorSubject({} as Filter);
  displayFilterSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private loadedTreesService: LoadedTreesService) {}

  getTrees() {
    this.loadedTreesService.loadTrees(this.filter);
  }

  setParams(validated: boolean, vlast: string[], type: string[]) {
    this.filter.validated = validated;
    this.filter.vlast = vlast;
    this.filter.type = type;

    // this.filterSubject.next(this.filter);
  }

  setDate(dateStart: string, dateEnd: string) {
    this.filter.dateStart = dateStart;
    this.filter.dateEnd = dateEnd;
  }

  setCoords(lon: number, lat: number, r: number) {
    this.filter.center = [lon, lat];
    this.filter.radius = r;
  }

  setAreaParams(kraj: string[], okres: string[], orp: string[], mzchu: string[], vzchu: string[]) {
    this.filter.kraj = kraj;
    this.filter.okres = okres;
    this.filter.orp = orp;
    this.filter.mzchu = mzchu;
    this.filter.vzchu = vzchu;

    // this.filterSubject.next(this.filter);
  }

  setPage(page: Page) {
    this.filter.start = page.pageIndex;
    this.filter.n = page.pageSize;
  }

  resetPage() {
    this.filter.start = 0;
    this.filter.n =  25;
    this.filterSubject.next(this.filter);
  }

}
