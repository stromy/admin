import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UserService } from './user.service';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  canActivate(): boolean {
    const currentUser = localStorage.getItem('role');
    this.userService.userSubject.next(currentUser as any);
    if (currentUser && currentUser !== '' && currentUser !== 'user') {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}