import { Injectable, OnDestroy, ApplicationRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Observer, Subject } from 'rxjs';

import {
  TreeI,
  Tree,
  LokalI,
  KategI,
  CommentI,
  OhroI,
  ClassificationSchema,
  ClassificationInterface,
  Dangers,
  Weights,
} from '../../lib';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ObserversModule } from '@angular/cdk/observers';

@Injectable()

export class EditTreeService implements OnDestroy {

  /** class keeping all informations about tree user is editing */
  public tSubject: BehaviorSubject<Tree> = new BehaviorSubject({} as Tree);

  /** matrix of classification elements user filled in */
  public kData: number[][] = [];

  /** structure of object holding texts in configuration file */
  public ConfKData: ClassificationInterface[] = ClassificationSchema;

  /** message returned from server */
  // public message: any;

  /** calculated values K and W */
  public kValuesSubject: Subject<number[]> = new Subject();

  public dangersMatrix: boolean[][] = [];

  constructor(
    private http: HttpClient
  ) {

  }

  initTree(T: Tree) {
    this.tSubject.next(T);
    this.initKData();
    this.initOData();
  }
  
  setTree(T: Tree) {
    this.tSubject.next(T);
  }

  setOhro(groupIndex: number, value: boolean[]) {
    if (typeof this.dangersMatrix[groupIndex] === 'undefined') {
      console.log('THIS INDEX DOES NOT EXISTS');
    } else {
      this.dangersMatrix[groupIndex] = value;
    }
  }

  /** get categ data from loaded tree and fit them into matrix for categ component */
  initKData() {
    let K1: number[] = this.tSubject.value.K.KATEG1.split(',').map((x) => {
      return parseInt(x, 10);
    });
    if (K1.length <= 1) {
      K1 = [0, 0, 0, 0, 0, 0, 0];
    }
    let K2 = this.tSubject.value.K.KATEG2.split(',').map((x) => {
      return parseInt(x, 10);
    });
    if (K2.length  <= 1) {
      K2 = [0, 0, 0];
    }
    let K3 = this.tSubject.value.K.KATEG3.split(',').map((x) => {
      return parseInt(x, 10);
    });
    if (K3.length  <= 1) {
      K3 = [0, 0];
    }
    let K4 = this.tSubject.value.K.KATEG4.split(',').map((x) => {
      return parseInt(x, 10);
    });
    if (K4.length  <= 1) {
      K4 = [0, 0, 0, 0];
    }
    let K5 = this.tSubject.value.K.KATEG5.split(',').map((x) => {
      return parseInt(x, 10);
    });
    if (K5.length  <= 1) {
      K5 = [0, 0, 0, 0, 0, 0];
    }
    this.kData = [K1, K2, K3, K4, K5];
    this.getKValues();
  }

  initOData() {
    let O1 = this.tSubject.value.O.OHRO1.split(',').map((x) => {
      return parseInt(x, 10) === 0 ? false : true;
    });
    if (O1.length <= 1) {
      O1 = [false, false, false];
    }
    let O2 = this.tSubject.value.O.OHRO2.split(',').map((x) => {
      return parseInt(x, 10) === 0 ? false : true;
    });
    if (O2.length  <= 1) {
      O2 = [false, false, false, false];
    }
    let O3 = this.tSubject.value.O.OHRO3.split(',').map((x) => {
      return parseInt(x, 10) === 0 ? false : true;
    });
    if (O3.length  <= 1) {
      O3 = [false, false, false];
    }
    let O4 = this.tSubject.value.O.OHRO4.split(',').map((x) => {
      return parseInt(x, 10) === 0 ? false : true;
    });
    if (O4.length  <= 1) {
      O4 = [false, false, false, false];
    }
    let O5 = this.tSubject.value.O.OHRO5.split(',').map((x) => {
      return parseInt(x, 10) === 0 ? false : true;
    });
    if (O5.length  <= 1) {
      O5 = [false, false, false, false];
    }
    this.dangersMatrix = [O1, O2, O3, O4, O5];
  }

  /** set a value of kData matrix
   * @param i index of section that should be edited
   * @param j index of subSection (question) that should be edited
   * @param value new value user select that should
   */
  setKData(i: number, j: number, value: number) {
    if (i >= this.kData.length) {
      throw new Error('[setKData] dim I doesnt fit');
    }
    if (j >= this.kData[i].length) {
      throw new Error('[setKData] dim J doesnt fit');
    }
    this.kData[i][j] = value;
  }
  /** get single data from kData matrix
   * @param i index of section that should be returned
   * @param j index of subSection (question) that should be returned
   * @returns selected number
   */
  getKData(i: number, j: number): number {
    if (i >= this.kData.length) {
      throw new Error('[getKData] dim I doesnt fit');
    }
    if (j >= this.kData[i].length) {
      throw new Error('[getKData] dim J doesnt fit');
    }
    return this.kData[i][j];
  }

  /** calculate values of all Classification categories */
  getKValues() {
    const V1 = this.kValue(this.kData[0], Weights.w1);
    const V2 = this.kValue(this.kData[1], Weights.w2);
    const V3 = this.kValue(this.kData[2], Weights.w3);
    const V4 = this.kValue(this.kData[3], Weights.w4);
    const V5 = this.kValue(this.kData[4], Weights.w5);
    this.kValuesSubject.next([V1, V2, V3, V4, V5]);
  }

  /** calculate category value (mark) dependent on weights
   * @param K values submitted by user
   * @param W weights from configuraton file
   */
  kValue(K: number[], W: number[]): number {
    let V = 0;
    for (let i = 0; i < K.length; i++) {
      V += +K[i] * W[i];
    }
    V = Math.round(V * 100) / 100;
    return V;
  }

  /** send form to backend and return observable of response */
  send(): Observable<any> {
    if (this.tSubject.value.S.DATIN instanceof Date) {
      this.tSubject.value.S.DATIN = `${this.tSubject.value.S.DATIN.getFullYear()}.${this.tSubject.value.S.DATIN.getMonth()}.${this.tSubject.value.S.DATIN.getDate()}`;
    }
    this.prepareData();
    const body = this.allTogether();
    const observable = new Observable(observer => {
      this.http
        .post(
          `${environment.server}/tree/updateTree`,
          body,
          {
            headers: new HttpHeaders().set('Content-Type', 'application/json'),
          }).subscribe(data => {
            observer.next(data);
            observer.complete();
          });
    });
    return observable;
  }

  /** form body before sending request to backend */
  allTogether(): string {
    const jsonBody = `
    {
      "strom":${JSON.stringify(this.tSubject.value.S)},
      "lokal":${JSON.stringify(this.tSubject.value.L)},
      "pisemneD":${JSON.stringify(this.tSubject.value.PD)},
      "obrazoveD":${JSON.stringify(this.tSubject.value.OD)},
      "kateg":${JSON.stringify(this.tSubject.value.K)},
      "comment":${JSON.stringify(this.tSubject.value.C)},
      "ohro":${JSON.stringify(this.tSubject.value.O)}
    }`;
    return jsonBody;
  }

  /** prepare data before forming body
   * classification matrix on strings and some hardcoded fields for now 
   */
  prepareData() {
    for (let i = 0; i < this.kData.length; i++) {
      let row = `${this.kData[i][0]}`;

      for (let j = 1; j < this.kData[i].length; j++) {
        row += `,${this.kData[i][j]}`;
      }
      this.tSubject.value.K[`KATEG${i + 1}`] = row;
    }
    for (let i = 0; i < this.dangersMatrix.length; i++) {
      let row = `${this.dangersMatrix[i][0] ? 1 : 0}`;

      for (let j = 1; j < this.dangersMatrix[i].length; j++) {
        row += `,${this.dangersMatrix[i][j] ? 1 : 0}`;
      }
      this.tSubject.value.O[`OHRO${i + 1}`] = row;
    }

    if (this.tSubject.value.S.DATAK instanceof Date) {
      this.tSubject.value.S.DATAK = `${this.tSubject.value.S.DATAK.getDate()}.${this.tSubject.value.S.DATAK.getMonth()}.${this.tSubject.value.S.DATAK.getFullYear()}`;
    }

    this.tSubject.value.L.X = this.tSubject.value.L.LON
    this.tSubject.value.L.Y = this.tSubject.value.L.LAT
  }

  ngOnDestroy() {
  }
}
