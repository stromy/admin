import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Identifiers } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Observer, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../interfaces/user.interface';

@Injectable()
export class UserService {
  userSubject: BehaviorSubject<'' | 'user' | 'admin' | 'superAdmin'>;

  editUser: User = {} as User;

  constructor(private http: HttpClient) {
    this.userSubject = new BehaviorSubject<'' | 'user' | 'admin' | 'superAdmin'>('');
  }

  public get currentUserRole(): string {
    return this.userSubject.value;
  }

  /** set user you are editing
   * @param user user to be editted
   */
  setEditUser(user: User) {
    this.editUser = user;
  }

  /** if there was any error with API request
   * @param error error returned from API request
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) { // client side or network
      console.error('An error occurred:', error.error.message);
    } else { // error returned by BE
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(`${error.error}`);  // return observable error
  }

  /** login user API request
   * @param nameEmail name or email of user
   * @param password password of user
   */
  login(nameEmail, password): Observable<any> {
    const payload = new HttpParams()
    .set('nameEmail', nameEmail)
    .set('password', password);
    return this.http
      .post( `${environment.server}/user/login`, payload, { withCredentials: true } )
      .pipe( catchError(this.handleError) )
      .pipe( map((data: any) => {
        if (!data.role && (data.role !== 'user' || data.role !== 'admin' || data.role !== 'superAdmin')) {
          console.error('wrong response for login from API')
          return;
        }
        localStorage.setItem('name', data.name);
        localStorage.setItem('role', data.role);
        this.userSubject.next(data.role);
      }));
  }

  changePassword(currentPassword, newPassword) {
    const payload = new HttpParams()
    .set('currentPassword', currentPassword)
    .set('newPassword', newPassword);
    return this.http
      .post( `${environment.server}/user/password`, payload, { withCredentials: true } )
      .pipe( catchError(this.handleError) )
      .pipe( map((data: any) => {
        if (!data.role && (data.role !== 'user' || data.role !== 'admin' || data.role !== 'superAdmin')) {
          console.error('wrong response for login from API')
          return;
        }
        localStorage.setItem('name', data.name);
        localStorage.setItem('role', data.role);
        this.userSubject.next(data.role);
      }));
  }

  /** kick me out please API */
  logout() {
    return this.http
    .post( `${environment.server}/user/logout`, new HttpParams(), { withCredentials: true } )
    .pipe(map(() => {
      localStorage.setItem('name', '');
      localStorage.setItem('role', '');
      this.userSubject.next('');
    }));
  }

  /** send register (or edit) request to API
   * @param user user to be registered (edited)
   */
  register(user: User) {
    let id = '';
    if (user.id) {
      id = user.id;
    }
    const payload = new HttpParams()
    .set('name', user.name)
    .set('email', user.email)
    .set('password', user.password)
    .set('phone', user.phone)
    .set('role', user.role)
    .set('id', id);
    return this.http
      .post( `${environment.server}/superAdmin/registerUser`, payload, { withCredentials: true } )
      .pipe( catchError(this.handleError) );
  }


  /** API to delete user with this id
   * @param id id of user to be deleted
   */
  deleteUser(id: string) {
    const payload = new HttpParams()
    .set('id', id.toString());
    return this.http
      .post( `${environment.server}/superAdmin/deleteUser`, payload, { withCredentials: true } )
      .pipe( catchError(this.handleError) );
  }
}
