import { Injectable } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { BehaviorSubject } from 'rxjs';
import { TreeMinimum } from 'src/lib';
import { environment } from '../../environments/environment';
import { FeatureCollection, Feature } from 'geojson';
import { getTreeMissingMatchingNodeDefError } from '@angular/cdk/tree';
// import { MapInitialized } from '../decorators/map';

@Injectable()
export class MapService {

  // ADD MARKERS
  // https://docs.mapbox.com/help/tutorials/markers-js/

  // treesGeoJson: Feature[] = [];

  map: mapboxgl.Map;
  mapLoaded: BehaviorSubject<boolean> = new BehaviorSubject(false);
  style = 'mapbox://styles/mapbox/streets-v11';
  lon = 14.484030;
  lat = 50.038420;
  zoom = 12;

  constructor() {
    mapboxgl.accessToken = environment.mapbox.accessToken;
  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: this.zoom,
      center: [this.lon, this.lat]
    });
    this.map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
    this.localizeUser();
    this.onLoad();
    // this.localizeUser();
  }

  onLoad() {
    this.map.loadImage(
      '/assets/map/tree-green.png', (error, image) => {
        this.map.addImage('tree-green', image);
      });
    this.map.loadImage(
      '/assets/map/tree-red.png', (error, image) => {
        this.map.addImage('tree-red', image);
      });
    this.map.on('load', () => {
      this.mapLoaded.next(true);
      this.addSource();
      this.addLayer();
      this.mapClick();
      this.mapMouseOver();
    });
  }

  getCurrentPosition(): [number, number] {
    return [this.map.getCenter().lng, this.map.getCenter().lat];
  }

  getZoom(): number {
    return this.map.getZoom();
  }

  placeTrees(trees: TreeMinimum[]) {
    const treesGeojsonFeatures = this.tree2geojson(trees);
    this.addData(treesGeojsonFeatures);
  }

  mapClick() {
    this.map.on('click', 'trees', (e) => {
      const feature = e.features[0];
      const coordinates = feature.geometry.coordinates.slice();
      const id = feature.properties.id;
      new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(
          '<h1>' + feature.properties.name + '</h1>' +
          '<div>typ objektu: ' + feature.properties.type + '</div>' +
          '<div>schválený: ' + feature.properties.validated + '</div>' +
          '<button id="mapPopup" value="' + feature.properties.id + '" data-validated="' + feature.properties.validated + '">OTEVŘÍT</button>'
        )
        .addTo(this.map);
    });
    this.map.on('click', 'clusters', (e) => {
      console.log('clustered tree clicked');
    });
  }

  /** change cursor to pointer when passed over icon or cluster */
  mapMouseOver() {
    this.map.on('mouseenter', 'trees', () => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', 'trees', () => {
      this.map.getCanvas().style.cursor = '';
    });
    this.map.on('mouseenter', 'clusters', () => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', 'clusters', () => {
      this.map.getCanvas().style.cursor = '';
    });
  }

  addSource() {
    this.map.addSource('trees', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: []
      },
      cluster: true,
      clusterMaxZoom: 22,
      clusterRadius: 50
    });
  }

  addData(treesData: FeatureCollection) {
    this.map.getSource('trees').setData(treesData);
  }

  addLayer() {
    this.map.addLayer({
      id: 'trees',
      type: 'symbol',
      source: 'trees',
      filter: ['!', ['has', 'point_count']],
      layout: {
        'icon-image': ['get', 'icon'],
        'icon-size': 0.14
      },
    });

    this.map.addLayer({
      id: 'clusters',
      type: 'circle',
      source: 'trees',
      filter: ['has', 'point_count'],
      paint: {
        // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
        // with three steps to implement three types of circles:
        //   * Blue, 20px circles when point count is less than 100
        //   * Yellow, 30px circles when point count is between 100 and 750
        //   * Pink, 40px circles when point count is greater than or equal to 750
        'circle-color': [
          'step',
          ['get', 'point_count'],
          '#51bbd6',
          100,
          '#f1f075',
          750,
          '#f28cb1'
        ],
        'circle-radius': [
          'step',
          ['get', 'point_count'],
          20,
          100,
          30,
          750,
          40
        ]
      }
    });

    this.map.addLayer({
      id: 'cluster-count',
      type: 'symbol',
      source: 'trees',
      filter: ['has', 'point_count'],
      layout: {
        'text-field': '{point_count_abbreviated}',
        'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
        'text-size': 12
      }
    });
  }

  tree2geojson(trees: TreeMinimum[]): FeatureCollection {
    const features: Feature[] = [];
    for (const tree of trees) {
      let icon = 'tree-green';
      if (tree.validated === 'false') {
        icon = 'tree-red';
      }
      features.push({
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [parseFloat(tree.lon), parseFloat(tree.lat)]
        },
        properties: {
          id: tree.id,
          icon,
          name: tree.name,
          type: tree.type,
          validated: tree.validated
        }
      });
    }
    return {
      type: 'FeatureCollection',
      features,
    };
  }

  localizeUser() {
    // Default HVO coordinates
    let lon = 14.484030;
    let lat = 50.038420;

    // Check for position permission and jump to current position if allowed
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        lon = position.coords.longitude;
        lat = position.coords.latitude;
        this.map.jumpTo({ center: { lon, lat } });
      });
    }
    this.map.jumpTo({
      center: {
        lng: lon,
        lat
      }
    });
  }

}
