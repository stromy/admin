import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { environment } from '../../../../environments/environment';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { ClassificationSchema, ClassificationInterface, Weights, Tree } from '../../lib';
import { Filter } from '../interfaces/filter.interface';

@Injectable()

export class LoadedTreesService implements OnDestroy {

  public treesSubject = new BehaviorSubject<Tree[]>([]);
  public currentTreeSubject = new BehaviorSubject<Tree>({} as Tree);
  public loadingSubject = new BehaviorSubject<boolean>(false);
  public validatedSubject = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
    // this.loadTrees();
  }

  getTreeById(id: string, validated: boolean) {
    if (id === '0' ) {
      this.currentTreeSubject.next({} as Tree);
      return;
    }
    this.http
      .post(
        `${environment.server}/tree/getById`,
        `id=${id}&getValide=${validated}`,
        {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
        }).subscribe((data: Tree) => {
          const loadedTree = this.parseTrees([data])[0];
          this.currentTreeSubject.next(loadedTree);
          return;
        });
    // }
  }

  /** take a filter, parse it to request body and send API request
   * @param filter request body parameters passed by user
   */
  loadTrees(filter: Filter) {
    this.loadingSubject.next(true);
    this.validatedSubject.next(filter.validated);
    const center: [number, number] = [filter.center[0], filter.center[1]];
    const body = `start=${filter.start}&n=${filter.n}&getValide=${filter.validated}&vlast=${JSON.stringify(filter.vlast)}&typObj=${JSON.stringify(filter.type)}&dateStart=${filter.dateStart}&dateEnd=${filter.dateEnd}&center=${JSON.stringify(center)}&radius=${filter.radius}`;
    this.http // get validated trees
      .post(
        `${environment.server}/admin/tree/getNTrees`,
        body,
        {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'), withCredentials: true
        })
      .subscribe(
        (data: any) => {
          const loadedTrees = this.parseTrees(data);
          this.loadingSubject.next(false);
          this.treesSubject.next(loadedTrees);
        });
  }

  parseTrees(data: any): Tree[] {
    if (!data) {
      return [];
    }
    const loadedTrees: Tree[] = [];
    for (const tree of data) {
      const T: Tree = {
        C: tree.comment,
        K: tree.kateg,
        L: tree.lokal,
        O: tree.ohro,
        OD: tree.pisemneD,
        PD: tree.obrazoveD,
        S: tree.strom,
        id: tree.strom.ID
      };
      loadedTrees.push(T);
    }
    return loadedTrees;
  }

  ngOnDestroy() {
    // console.log('Im destroyed');
  }
}
