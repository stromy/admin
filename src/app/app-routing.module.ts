import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// COMPONENTS
import { EditTreeComponent } from './components/edit-tree/edit-tree.component';
import { TreesTableComponent } from './components/trees-table/trees-table.component';
import { PrintTreeComponent } from './components/print-tree/print-tree.component';
import { DownloadsComponent } from './components/downloads/downloads.component';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { RegisterComponent } from './components/users/register/register.component';
import { LoginComponent } from './components/users/login/login.component';
import { PasswordComponent } from './components/users/password/password.component';
import { MapComponent } from './components/map/map.component';
import { AuthGuard } from './services/auth.guard';
import { AdminGuard } from './services/admin.guard';

// SERVICES

const routes: Routes = [
  {
    path: '',
    component: MapComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'trees',
    component: TreesTableComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'editTree/:id/:validatedSet',
    component: EditTreeComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'print',
    component: PrintTreeComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'password',
    component: PasswordComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'register',
    redirectTo: 'register/',
    pathMatch: 'full'
  }, {
    path: 'register/:id',
    component: RegisterComponent,
    canActivate: [AdminGuard]
  }, {
    path: 'users',
    component: ListUsersComponent,
    canActivate: [AdminGuard]
  }, {
    path: 'downloads',
    component: DownloadsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
