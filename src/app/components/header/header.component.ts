import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  header = 'Aplikace významné stromy';

  constructor(
    private router: Router,
    private cd: ChangeDetectorRef
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.getHeader(event.url);
        this.cd.detectChanges();
      }
    });
  }

  ngOnInit() {

  }

  getHeader(url: string) {
    const path = url.split('/');
    switch (path[1]) {
      case 'trees':
        this.header = 'Tabulka stromů';
        break;
      case '':
        this.header = 'Mapa stromů';
        break;
      case 'editTree':
        this.header = 'Úprava stromu';
        break;
      case 'print':
        this.header = 'Tisk stromu';
        break;
      case 'login':
        this.header = 'Přihlášení uživatele';
        break;
      case 'password':
        this.header = 'Změna hesla';
        break;
      case 'register':
        this.header = 'Formulář uživatele';
        break;
      case 'users':
        this.header = 'Seznam uživatelů';
        break;
      case 'downloads':
        this.header = 'Export databáze stromů';
        break;
    }
  }


}
