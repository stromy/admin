import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../../interfaces/user.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  /** was validation successfull */
  valide = true;

  /** message about users error */
  validatorMessage = 'pole s hvězdičkou musejí být vyplněna';

  /** id provided in route */
  id = '';

  constructor(
    private formBuilder: FormBuilder,
    private userß: UserService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.initForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.initForm();
      if (params.id === '') {
        this.id = '';
        this.cdr.detectChanges();
      } else if ( this.userß.editUser.id && params.id === this.userß.editUser.id.toString()) {
        this.setUser(this.userß.editUser);
      }
    });
  }

  /** if id found in route, set this user to be edited
   * @param user user which should be edited
   */
  setUser(user: User) {
    this.registerForm.get('name').setValue(user.name);
    this.registerForm.get('email').setValue(user.email);
    this.registerForm.get('phone').setValue(user.phone);
    if (user.role === 'admin') {
      this.registerForm.get('role').setValue(true);
    } else {
      this.registerForm.get('role').setValue(false);
    }
    this.id = user.id;
    this.registerForm.get('passwords').get('password').clearValidators();
    this.registerForm.get('passwords').get('passwordAgain').clearValidators();
  }

  /** set all form controlls to "null" format */
  initForm() {
    this.validatorMessage = 'pole s hvězdičkou musejí být vyplněna';
    this.valide = true;
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email] ],
      phone: [''],
      role: [''],
      passwords: this.formBuilder.group({
        password: [''],
        passwordAgain: [''],
      }, { validator: this.passwordConfirming.bind(this) })
    });
    this.registerForm.get('passwords').get('password').setValidators(Validators.required)
    this.registerForm.get('passwords').get('passwordAgain').setValidators(Validators.required)
  }

  /** reset validators errors */
  resetErrors() {
    this.registerForm.reset();
    this.registerForm.get('name').setErrors(null);
    this.registerForm.get('email').setErrors(null);
    this.registerForm.get(['passwords', 'password']).setErrors(null);
    this.registerForm.get(['passwords', 'passwordAgain']).setErrors(null);
    this.registerForm.get('phone').setErrors(null);
    this.cdr.detectChanges();
  }

  /** validation if provaded passwords are same */
  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('password').value !== c.get('passwordAgain').value) {
      this.valide = false;
      this.validatorMessage = 'Hesla se musí shodovat';
      return { invalid: true };
    }
    this.valide = true;
  }

  /** check validators and submit user */
  submit() {
    if (this.registerForm.valid) {  // form is valide
      const user: User = {} as User;
      user.name = this.registerForm.get('name').value;
      user.email = this.registerForm.get('email').value;
      user.password = this.registerForm.get(['passwords', 'password']).value;
      user.phone = this.registerForm.get('phone').value;
      user.id = this.id;
      let role = 'user';
      if (this.registerForm.get('role').value) {
        role = 'admin';
      }
      user.role = role;
      this.userß.register(user)
        .subscribe(
          () => {
            if (this.id !== '') {
              this.router.navigate(['/users']);
            }
            this.resetErrors();
          },
          error => this.processError(error.trim())
        );
    } else {  // form not valide
      this.valide = false;
      this.validatorMessage = 'Všechna pole s hvězdičkou musí být vyplněna';
    }
  }

  /** process error returned from API
   * @param error error from API
   */
  processError(error: string) {
    this.valide = false;
    switch (error) {
      case 'duplicate name': {
        this.validatorMessage = 'Toto jméno již existuje, zkuste jiné';
        break;
      }
      case 'duplicate email': {
        this.validatorMessage = 'Tento email již existuje, zkuste jiný';
        break;
      }
      default: {
        this.validatorMessage = 'Něco je špatně, nevím co, kontaktujte správce';
        console.error(error);
      }
    }
    this.cdr.detectChanges();
  }
}
