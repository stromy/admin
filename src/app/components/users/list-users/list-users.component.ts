import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../../../interfaces/user.interface';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  users: User[] = [];

  loadingData = false;

  userRole: '' | 'user' | 'admin' | 'superAdmin';

  selectedUsers: boolean[] = [];
  selectAll: boolean;

  constructor(
    private http: HttpClient,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private userß: UserService
  ) { }

  ngOnInit() {
    this.getUsers();
    this.userRole = this.userß.userSubject.value;
  }

  getUsers() {
    this.loadingData = true;
    this.http
      .post(`${environment.server}/superAdmin/getAllUsers`, new HttpParams(), { withCredentials: true })
      .subscribe((users: User[]) => {
        this.users = users;
        this.selectedUsers = [];
        for (const user of users) {
          this.selectedUsers.push(false);
        }
        this.loadingData = false;
        this.cdr.detectChanges();
      });
  }

  edit(user: User) {
    this.userß.setEditUser(user);
    this.router.navigate(['/register', user.id]);
  }

  delete(id: string) {
    this.userß.deleteUser(id).subscribe(
      () => { this.getUsers(); },
      (error) => { console.error(error); }
    );
  }

  /** activate / deactivate all items checkboxes */
  toggleAll() {
    this.selectAll = !this.selectAll;
    for (let i = 0; i < this.selectedUsers.length; i++) {
      this.selectedUsers[i] = this.selectAll;
    }
  }

}
