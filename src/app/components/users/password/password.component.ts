import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { CdkRow } from '@angular/cdk/table';

@Component({
  selector: 'app-password',
  templateUrl: 'password.component.html',
  styleUrls: ['password.component.scss']
})
export class PasswordComponent {
  constructor(
    private userß: UserService,
    private router: Router,
    private cdr: ChangeDetectorRef) {
  }

  changePasswordForm: FormGroup;
  wrongRep = false;
  wrongCurrent = false;

  ngOnInit() {
    this.changePasswordForm = new FormGroup({
      currentPassword: new FormControl(null),
      newPassword: new FormControl(null),
      passwordCheck: new FormControl(null),
    });
  }

  submit() {
    const currentPassword = this.changePasswordForm.get('currentPassword').value;
    const newPassword = this.changePasswordForm.get('newPassword').value;
    const newPasswordAgain = this.changePasswordForm.get('passwordCheck').value;
    if(newPassword === newPasswordAgain) {
      this.wrongRep = false;
    } else {
      this.wrongRep = true;
      return;
    }

    this.userß.changePassword(currentPassword, newPassword)
      .subscribe(
        () => {
          this.wrongCurrent = false;
          this.router.navigate(['/']);
        },
        error => {
          if (error === '401') {
            this.wrongCurrent = true;
            this.cdr.detectChanges();
          }
        });
  }
}