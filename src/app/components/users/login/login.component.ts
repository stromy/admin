import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { CdkRow } from '@angular/cdk/table';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  wrongAuth = false;

  constructor(
    private userß: UserService,
    private router: Router,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      name: new FormControl(null),
      password: new FormControl(null)
    });
  }

  submit() {
    const nameEmail = this.loginForm.get('name').value;
    const password = this.loginForm.get('password').value;

    this.userß.login(nameEmail, password)
      .subscribe(
        () => {
          this.wrongAuth = false;
          this.router.navigate(['/']);
        },
        error => {
          if (error === '401') {
            this.wrongAuth = true;
            this.cdr.detectChanges();
          }
        });
  }

}
