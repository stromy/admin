import { ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { MapService } from '../../services/map.service';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeMinimum } from '../../../lib/interfaces/tree.interface';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  trees: TreeMinimum[] = [];
  mapLoadedSubscribtion: Subscription;


  constructor(
    private map: MapService,
    private http: HttpClient,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.map.buildMap();
    this.mapLoadedSubscribtion = this.map.mapLoaded.subscribe(loaded => {
      if (loaded) {
        this.getTreesAround();
      }
    });
  }

  @HostListener('window:click', ['$event.target'])
  openTree(target) {
    if (target.id === 'mapPopup') {
      this.router.navigate(['/editTree', target.value, target.dataset.validated])
    }
  }

  getTreesAround() {
    const coords = this.map.getCurrentPosition();
    let distance = this.getDistance(coords[1]);
    distance = Math.floor(distance * 2);  // just so user can move map without reloading trees
    const body = `center=${JSON.stringify(coords)}&radius=${distance}`;
    return this.http
      .post(`${environment.server}/tree/getTreesAround`,
        body,
        {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        })
      .subscribe((trees: TreeMinimum[]) => {
        if (!trees || trees.length === 0) {
          this.snackBar.open(
            'V tomto okolí nejsou žádné stromy, zkuste hledat jinde',
            '',
            { duration: 2000 });
          return;
        }
        trees.sort((a, b) => {
          return a.distance - b.distance;
        });
        this.trees = trees;
        this.map.placeTrees(trees);
        this.cdr.detectChanges();
      });
  }

  // calculate distance in which trees should be loaded
  getDistance(lat: number): number {
    const zoom = this.map.getZoom();
    const mPerPixel = this.metersPerPixel(lat, zoom);
    return mPerPixel * window.innerWidth;
  }

  /** calculate pixel meter ratio
   * @param latitude latitude we are in
   * @param zoomLevel current zoom level
   */
  private metersPerPixel(latitude, zoomLevel) {
    const earthCircumference = 40075017;
    const latitudeRadians = latitude * (Math.PI / 180);
    return earthCircumference * Math.cos(latitudeRadians) / Math.pow(2, zoomLevel + 9);
  }

  ngOnDestroy() {
    this.mapLoadedSubscribtion.unsubscribe();
  }

}
