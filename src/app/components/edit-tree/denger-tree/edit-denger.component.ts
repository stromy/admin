import { Component, EventEmitter, OnInit} from '@angular/core';
import { Dangers } from '../../../../lib';
import { EditTreeService } from '../../../services/edit-tree.service';
import { ClassificationInterface } from '../../../../lib';

@Component({
  selector: 'app-edit-denger',
  templateUrl: './edit-denger.component.html',
  styleUrls: ['./edit-denger.component.scss']
})
export class EditDengerComponent implements OnInit {

  dangerStructure = Dangers;
  dangersMatrix: boolean[][];

  constructor(
    public editTreeService: EditTreeService,
    // public cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.dangersMatrix = this.editTreeService.dangersMatrix;
    // this.configData = this.editTreeService.ConfKData;
    // this.editTreeService.ohroValuesSubject.subscribe((data: number[]) => {
    //   this.kValues = data;
    //   this.cdr.detectChanges()

    // });
  }

}
