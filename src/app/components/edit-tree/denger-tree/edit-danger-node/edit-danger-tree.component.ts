import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { EditTreeService } from '../../../../services/edit-tree.service';

@Component({
  selector: 'app-edit-danger-node',
  templateUrl: './edit-danger-node.component.html',
  styleUrls: ['./edit-danger-node.component.scss']
})
export class EditDangerNodeComponent implements OnInit {
  @Input() data: any;
  @Input() groupIndex: number;
  @Input() values: boolean[];
  folded = false;
  grades: boolean[] = [];
  mainChecked: boolean;
  mainIndeterminate: boolean;

  constructor(
    private editTreeService: EditTreeService,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    for (const grade of this.data.value.grades) {
      this.grades.push(false);
    }
    if (this.grades.length === this.values.length) {
      this.grades = this.values;
    }
    this.checkAll();
  }

  change() {
    this.editTreeService.setOhro(this.groupIndex, this.grades);
    this.checkAll();
  }

  fold() {
    this.folded = !this.folded;
  }

  changeAll() {
    this.mainChecked = !this.mainChecked;
    this.mainIndeterminate = false;
    for (const i of Object.keys(this.grades)) {
      this.grades[i] = this.mainChecked;
    }
    this.editTreeService.setOhro(this.groupIndex, this.grades);
  }

  // check all subcheckboxes and if all are checked, return true
  checkAll() {
    let allChecked = true;
    let noneChecked = false;

    for (const grade of this.grades) {
      if (grade) {
        noneChecked = true;
      } else {
        allChecked = false;
      }
    }

    if (allChecked) {  // all checkboxes are checked
      this.mainIndeterminate = false;
      this.mainChecked = true;
    } else {
      this.mainChecked = false;
      if (noneChecked) { // non of the checkboxes is checked
        this.mainIndeterminate = true;
      } else {  // some are checked
        this.mainIndeterminate = false;
      }
    }

  }



}
