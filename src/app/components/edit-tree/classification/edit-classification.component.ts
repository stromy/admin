import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EditTreeService } from '../../../services/edit-tree.service';
import { ClassificationInterface } from '../../../../lib';


@Component({
  selector: 'app-edit-classification',
  templateUrl: './edit-classification.component.html',
  styleUrls: ['./edit-classification.component.scss']
})

export class EditClassificationComponent implements OnInit {
  public kData: number[][];
  public configData: ClassificationInterface[];
  public kValues: number[] = [0, 0, 0, 0, 0];

  constructor(
    public editTreeService: EditTreeService,
    public cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.configData = this.editTreeService.ConfKData;
    this.editTreeService.kValuesSubject.subscribe((data: number[]) => {
      this.kValues = data;
      this.cdr.detectChanges()
    });
  }



}
