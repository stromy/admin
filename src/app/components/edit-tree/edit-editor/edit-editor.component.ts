import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
import { Dangers } from '../../../../lib';
import { EditTreeService } from '../../../services/edit-tree.service';
import { ClassificationInterface } from '../../../../lib';
import { TreeTypes, Tree } from '../../../../lib';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-editor',
  templateUrl: './edit-editor.component.html',
  styleUrls: ['./edit-editor.component.scss']
})

export class EditEditorComponent implements OnInit, OnDestroy {
  T: Tree;
  adminFormGroup: FormGroup = this.formBuilder.group({
    comment: ['', Validators.maxLength(300)],
    files: ['']
  });

  treeSubscription: Subscription;
 
  constructor(
    public formBuilder: FormBuilder,
    public editTreeService: EditTreeService,
    public cd: ChangeDetectorRef
  ) {
  }
  ngOnInit() {
    this.treeSubscription = this.editTreeService.tSubject.subscribe((t: Tree) => {
      this.T = t;
      this.adminFormGroup.setValue({
        comment: this.T.C.COM_A,
        files: ''
      });
      this.cd.detectChanges();
    });
    this.adminFormGroup.valueChanges.subscribe(() => {
      this.editTreeService.tSubject.value.C.COM_A = this.adminFormGroup.get('comment').value;
      this.editTreeService.tSubject.value.PD.URL = this.adminFormGroup.get('files').value;
    });
  };
  ngOnDestroy() {
    this.treeSubscription.unsubscribe()
  }
}
