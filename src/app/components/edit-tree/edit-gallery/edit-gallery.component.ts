import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
import { Dangers } from '../../../../lib';
import { EditTreeService } from '../../../services/edit-tree.service';
import { ClassificationInterface } from '../../../../lib';
import { TreeTypes, Tree } from '../../../../lib';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-gallery',
  templateUrl: './edit-gallery.component.html',
  styleUrls: ['./edit-gallery.component.scss']
})
export class EditGalleryComponent implements OnInit, OnDestroy {
  T: Tree;
  imagesUrl: string[] = []
  treeSubscription: Subscription;

  constructor(
    public editTreeService: EditTreeService,
    public cd: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.treeSubscription = this.editTreeService.tSubject.subscribe((t: Tree) => {
      this.T = t;
      if (this.T.OD.URL && this.T.OD.URL !== '') {
        this.imagesUrl = this.T.OD.URL.split(',')
        for (let i in this.imagesUrl) {
          this.imagesUrl[i] = `${environment.server}/tree/uploads/obrazove/${this.imagesUrl[i]}`
        }
      }
      this.cd.detectChanges();
    });
  }
  ngOnDestroy() {
    this.treeSubscription.unsubscribe()
  }
}
