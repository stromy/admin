import { Component, EventEmitter, Input, OnInit, AfterViewChecked, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Dangers } from '../../../../lib';
import { EditTreeService } from '../../../services/edit-tree.service';
import { ClassificationInterface } from '../../../../lib';
import { TreeTypes, Tree } from '../../../../lib';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

interface SelectInterface {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-edit-primary',
  templateUrl: './edit-primary.component.html',
  styleUrls: ['./edit-primary.component.scss']
})
export class EditPrimaryComponent implements OnInit, OnDestroy {

  baseFormGroup: FormGroup = this.formBuilder.group({
    lonCtrl: [''],
    latCtrl: [''],
    typeCtrl: [''],
    commUCtrl: [{ value: '', disabled: true }],
    dateAktCtrl: ['']
  });
  objectTypes: SelectInterface[] = [];
  T: Tree;
  treeSubscription: Subscription;

  constructor(
    public editTreeService: EditTreeService,
    public formBuilder: FormBuilder,
    public cd: ChangeDetectorRef
  ) {
    for (const type of TreeTypes) { // input select types
      this.objectTypes.push({ value: type, viewValue: type });
    }
  }

  ngOnInit() {
    this.treeSubscription = this.editTreeService.tSubject.subscribe((t: Tree) => {
      if (!t) {
        return;
      }
      this.T = t;
      this.baseFormGroup.setValue({
        lonCtrl: this.T.L.LON,
        latCtrl: this.T.L.LAT,
        typeCtrl: this.T.S.TYP_OBJ,
        commUCtrl: this.T.C.COM_U,
        dateAktCtrl: this.T.S.DATAK
      });
      if (this.T.id === 0) {
        this.getLocation();
      }
      this.cd.detectChanges();
    });

    this.baseFormGroup.valueChanges.subscribe(() => {
      this.editTreeService.tSubject.value.L.LON = this.baseFormGroup.get('lonCtrl').value;
      this.editTreeService.tSubject.value.L.LAT = this.baseFormGroup.get('latCtrl').value;
      this.editTreeService.tSubject.value.S.TYP_OBJ = this.baseFormGroup.get('typeCtrl').value;
      this.editTreeService.tSubject.value.S.DATAK = this.baseFormGroup.get('dateAktCtrl').value;
    });
  }

  /** get device location and set it in service */
  getLocation() {
    if (this.T.L.LAT !== '' || this.T.L.LON !== '') {
      return;
    }
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const lon = position.coords.longitude.toString();
        const lat = position.coords.latitude.toString();
        this.baseFormGroup.controls.lonCtrl.setValue(lon);
        this.baseFormGroup.controls.latCtrl.setValue(lat);
      });
    } else {
      console.log('No support for geolocation');
    }
  }

  ngOnDestroy() {
    this.treeSubscription.unsubscribe()
  }

}
