import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadedTreesService } from 'src/app/services/loaded-trees.service';
import { TreeTypes, Tree } from '../../../lib';
import { EditTreeService } from 'src/app/services/edit-tree.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-edit-tree',
  templateUrl: './edit-tree.component.html',
  styleUrls: ['./edit-tree.component.scss']
})
export class EditTreeComponent implements OnInit {
  // @Input() date;
  tree: Tree;
  treeId: number;
  validatedSet: boolean;

  editTreeRouter = 'primary';

  treeSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private loadedTreesService: LoadedTreesService,
    private editTreeService: EditTreeService,
    private router: Router
  ) {
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => {
        this.treeId = params.id;
        this.validatedSet = JSON.parse(params.validatedSet);
        this.loadedTreesService.getTreeById(params.id, this.validatedSet);
      }
    );
    this.treeSubscription = this.loadedTreesService.currentTreeSubject.subscribe((tree: Tree) => {
      if (!tree || Object.keys(tree).length === 0) {
        tree = new Tree();
      }
      this.tree = tree;
      this.tree.S.DATAK = new Date();
      this.editTreeService.initTree(tree);
    });
  }

  submitTree() {

    this.editTreeService.setTree(this.tree);
    this.editTreeService.send().subscribe(data => {
      if (data.status === 'ok') {
        if (confirm('chcete pokračovat v editaci tohoto stromu?')) {
        } else {
          this.router.navigate(['/']);
        }

      } else {
        alert(`Vyskytla se chyba!, zpráva je: ${data.message}`);
      }
    });
  }

}
