import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  roleSubscription: Subscription;
  userRole: '' | 'user' | 'admin' | 'superAdmin';
  active = '';

  constructor(
    private userß: UserService,
    private router: Router,
    private cdr: ChangeDetectorRef
    ) {
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          this.getRoute(event.url);
          this.cdr.detectChanges();
        }
      });
    }

  ngOnInit() {
    this.roleSubscription = this.userß.userSubject.subscribe(role => {
      this.userRole = role;
      this.cdr.detectChanges();
    });
  }

  logout() {
    this.userß.logout().subscribe(data => {
      this.router.navigate(['/login']);
    });
  }

  getRoute(url: string) {
    const path = url.split('/');
    switch (path[1]) {
      case 'trees':
        this.active = 'trees';
        break;
      case '':
        this.active = '';
        break;
      case 'editTree':
        this.active = 'trees';
        break;
      case 'print':
        this.active = 'trees';
        break;
      case 'login':
        this.active = 'user';
        break;
      case 'password':
        this.active = 'user';
        break;
      case 'register':
        this.active = 'user';
        break;
      case 'users':
        this.active = 'user';
        break;
      case 'downloads':
        this.active = 'downloads';
        break;
    }
  }

}
