
import { FilterService } from 'src/app/services/filter.service';
import { Filter, Page, Kraj, Okres, Zchu, Orp } from '../../../interfaces/filter.interface';
import { AfterViewInit, Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
// import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { ClassificationSchema, ClassificationInterface, Weights, Tree } from '../../../lib';
import { LoadedTreesService } from '../../../services/loaded-trees.service';
// import { TableDataService } from '../table-data.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { ClassificationSchema, ClassificationInterface, Weights, Tree } from '../../../../lib';

@Component({
  selector: 'app-filter-paginator',
  templateUrl: 'filter-paginator.component.html',
  styleUrls: ['filter-paginator.component.scss']
})

export class FilterPaginatorComponent implements OnInit, OnDestroy {

  /** source for table */
  treesSubscription: Subscription;

  filterSubscription: Subscription;

  page: Page = { pageIndex: 0, pageSize: 25, length: 50 };
  pageEvent: PageEvent;

  constructor(
    private http: HttpClient,
    public loadedTreesService: LoadedTreesService,
    private cdr: ChangeDetectorRef,
    // private tableDataß: TableDataService,
    private filterService: FilterService
  ) {

  }

  ngOnInit() {
    this.filterSubscription = this.filterService.filterSubject.subscribe((filter: Filter) => {
      this.page.pageIndex = filter.start;
      this.page.pageSize = filter.n;
      this.cdr.detectChanges();
    });

    this.treesSubscription = this.loadedTreesService.treesSubject.subscribe((trees: Tree[]) => {
      // update paginator
      if(!this.page.pageSize) { this.page.pageSize = 25 }
      if(!this.page.pageIndex) { this.page.pageIndex = 0 }
      if (trees.length < this.page.pageSize) {
        this.page.length = this.page.pageIndex * this.page.pageSize + trees.length;
      } else {
        this.page.length = (this.page.pageIndex+1) * this.page.pageSize + this.page.pageSize;
        // this.page.length += this.page.pageSize;
      }
      this.cdr.detectChanges();
    });

  }

  changePage(currentPage: PageEvent) {
    this.pageEvent = currentPage;
    this.page.pageIndex = currentPage.pageIndex;
    this.page.pageSize = currentPage.pageSize;
    this.filterService.setPage(this.page);
    this.filterService.getTrees();
  }

  ngOnDestroy() {
    this.treesSubscription.unsubscribe();
    this.filterSubscription.unsubscribe();
  }

}
