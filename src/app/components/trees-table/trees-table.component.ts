import { AfterViewInit, Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ClassificationSchema, ClassificationInterface, Weights, Tree } from '../../../lib';
import { LoadedTreesService } from '../../services/loaded-trees.service';
import { Subscription } from 'rxjs';
import { FilterService } from 'src/app/services/filter.service';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-trees-table',
  templateUrl: './trees-table.component.html',
  styleUrls: ['./trees-table.component.scss'],
  providers: []
})
export class TreesTableComponent implements OnInit, OnDestroy {

  treesSubscription: Subscription;
  showFilterSubscription: Subscription;
  showFilter: boolean;

  trees: Tree[] = [];

  validatedSubscription: Subscription;
  showValidated: boolean;
  // to skip init of treesSubject and display spinner at first load
  // initLoad = true;
  loadingData = false;
  loadingSubscription: Subscription;

  /** interface schema for categorization */
  public schema: ClassificationInterface[] = ClassificationSchema;

  selectedTrees: boolean[] = [];
  selectAll: boolean;

  constructor(
    public loadedTreesService: LoadedTreesService,
    // private tableDataß: TableDataService,
    private cdr: ChangeDetectorRef,
    private filterService: FilterService,
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    // this.loadingData = true;
    this.filterService.getTrees();
    this.treesSubscription = this.loadedTreesService.treesSubject.subscribe((trees: Tree[]) => {
      this.trees = trees;
      console.log(trees)
      this.selectedTrees = [];
      for (const tree of trees) {
        this.selectedTrees.push(false);
      }
      this.cdr.detectChanges();
    });
    this.showFilterSubscription = this.filterService.displayFilterSubject.subscribe(show => {
      this.showFilter = show;
    });

    this.loadingSubscription = this.loadedTreesService.loadingSubject.subscribe((loading: boolean) => {
      this.loadingData = loading;
      this.cdr.detectChanges();
    });

    this.validatedSubscription = this.loadedTreesService.validatedSubject.subscribe((validated: boolean) => {
      this.showValidated = validated;
      this.cdr.detectChanges();
    });
  }

  prepareDate(date: string): string {
    return date.split(' ')[0];
  }

  getKValues(tree: Tree): number[] {
    const kArr: number[] = [
      this.kValue(tree.K.KATEG1, Weights.w1),
      this.kValue(tree.K.KATEG2, Weights.w2),
      this.kValue(tree.K.KATEG3, Weights.w3),
      this.kValue(tree.K.KATEG4, Weights.w4),
      this.kValue(tree.K.KATEG5, Weights.w5)
    ];
    let kValue = 0;
    for (let j = 0; j < kArr.length; j++) {
      kValue += kArr[j] * Weights.W[j];
    }
    kValue = Math.round(kValue * 100) / 100;
    return [kValue, kArr[0], kArr[1], kArr[2], kArr[3], kArr[4]];
  }

  /** calculate category value (mark) dependent on weights
   * @param K values submitted by user
   * @param W weights from configuraton file
   */
  kValue(K: string, W: number[]): number {
    const k = K.split(',');
    if (k.length !== W.length) {
      return 0;
    }
    let V = 0;
    for (let i = 0; i < k.length; i++) {
      V += +k[i] * W[i];
    }
    V = Math.round(V * 100) / 100;
    return V;
  }

  /** send post request to backend to remove tree from database */
  delete(id: string) {
    const payload = new HttpParams()
      .set('id', id);
    this.http
      .post(`${environment.server}/admin/tree/deleteTree`, payload, { withCredentials: true })
      .subscribe((data: any) => {
        if (data.status === 'ok') {
          this.filterService.getTrees();
        }
      });
  }

  validate(id: string) {
    const payload = new HttpParams()
      .set('id', id);
    this.http
      .post(`${environment.server}/admin/tree/validateTree`, payload, { withCredentials: true })
      .subscribe((data: any) => {
        if (data.status === 'ok') {
          this.filterService.getTrees();
        }
      });
  }

  toggleFilter() {
    this.filterService.displayFilterSubject.next(true);
  }

  /** calculate opacity of background color for k td
   * @param K value of K usually getKValues(tree)[0]
   */
  getColor(K: number) {
    return {'background-color': `rgba(255, 190, 196,${K / 60 + 0.1})`};
  }

  /** prepare tooltip text
   * @param K array of all K, length 5, usually getKValues(tree)
   */
  getTooltip(K: any) {
    return `K=${K[0]};\nK1=${K[1]};\nK2=${K[2]};\nK3=${K[3]};\nK4=${K[4]};\nK5=${K[5]}`;
  }

  ngOnDestroy() {
    this.treesSubscription.unsubscribe();
    this.showFilterSubscription.unsubscribe();
    this.loadingSubscription.unsubscribe();
    this.validatedSubscription.unsubscribe();
  }

  /** activate / deactivate all items checkboxes */
  toggleAll() {
    this.selectAll = !this.selectAll;
    for (let i = 0; i < this.selectedTrees.length; i++) {
      this.selectedTrees[i] = this.selectAll;
    }
  }

}


