import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FilterService } from 'src/app/services/filter.service';
import { Filter, Page, Kraj, Okres, Zchu, Orp } from '../../../interfaces/filter.interface';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoadedTreesService } from 'src/app/services/loaded-trees.service';
import { Subscription } from 'rxjs';
import { CdkRow } from '@angular/cdk/table';
import { areas } from './areas';

@Component({
  selector: 'app-filter',
  templateUrl: 'filter.component.html',
  styleUrls: ['filter.component.scss']
})

export class FilterComponent implements OnInit {
  validatedCtrl = new FormControl();
  vlastCtrl = new FormControl();
  typeCtrl = new FormControl();
  dateRange = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });
  lonCtrl = new FormControl();
  latCtrl = new FormControl();
  rCtrl = new FormControl();

  orpCtrl = new FormControl();
  kodokCtrl = new FormControl();
  krajCtrl = new FormControl();
  mZchuCtrl = new FormControl();
  vZchuCtrl = new FormControl();

  vlastItems: string[] = [];
  typeItems: string[] = ['jednotlivec', 'alej', 'stromořadí', 'skupina'];

  loadingData = false;
  loadingSubscription: Subscription;

  areas = areas;

  constructor(
    private filterService: FilterService,
    private http: HttpClient,
    private loadedTreesService: LoadedTreesService,
    private cdr: ChangeDetectorRef
    ) {
    this.loadVlast();
  }

  ngOnInit() {
    this.loadingSubscription = this.loadedTreesService.loadingSubject.subscribe((loading: boolean) => {
      this.loadingData = loading;
      this.cdr.detectChanges();
    });
  }

  // read formControlls, get and parse values, save them to filter
  processFilter() {
    const dateRange = this.prepareDate();
    this.filterService.setParams(this.validatedCtrl.value, this.vlastCtrl.value, this.typeCtrl.value);
    this.filterService.setDate(dateRange[0], dateRange[1]);
    this.filterService.setCoords(this.lonCtrl.value, this.latCtrl.value, this.rCtrl.value);
  }

  prepareDate(): [string, string] {
    let dateStart = '';
    let dateEnd = '';
    if (this.dateRange.get('start').value !== null && this.dateRange.get('end').value !== null) {
      dateStart = this.parseFormDate(this.dateRange.get('start').value) + ' 00:00:00';
      dateEnd =  this.parseFormDate(this.dateRange.get('end').value) + ' 23:59:00';
    }
    return [dateStart, dateEnd];
  }

  parseFormDate(date: string): string {
    const dateArray = date.toString().split(' ');
    const month = new Date(dateArray[1] + '-1-01').getMonth() + 1;
    const day = dateArray[2];
    const year = dateArray[3];
    const parsedDate = year + '-' + ('0' + month).slice(-2) + '-' + day;
    return parsedDate;
  }

  submitCtrlForms() {
    this.processFilter();
    this.filterService.getTrees();
    this.filterService.resetPage();
    this.filterService.displayFilterSubject.next(false);
  }

  clearForm() {
    this.filterService.displayFilterSubject.next(false);
  }

  loadVlast() {
    this.http
    .get(`${environment.server}/tree/getAllVlast`)
    .subscribe((data: any) => {
        if (data.status === 'ok') {
          this.vlastItems = data.message;
        }
        return;
      });
  }

  hideFilter() {
    this.filterService.displayFilterSubject.next(false);
  }

}
