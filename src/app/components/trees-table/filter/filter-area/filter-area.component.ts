import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FilterService } from 'src/app/services/filter.service';
import { Filter, Page, Kraj, Okres, Zchu, Orp } from '../../../../interfaces/filter.interface';

@Component({
  selector: 'app-filter-area',
  templateUrl: 'filter-area.component.html',
  styleUrls: ['filter-area.component.scss']
})

export class FilterAreaComponent {
  krajCtrl = new FormControl();
  okresCtrl = new FormControl();
  mzchuCtrl = new FormControl();
  vzchuCtrl = new FormControl();
  orpCtrl = new FormControl();

  krajItems: Kraj[] = [];
  okresItems: Okres[] = [];
  orpItems: Orp[] = [];
  mzchuItems: Zchu[] = [];
  vzchuItems: Zchu[] = [];

  constructor(private filterService: FilterService) {}
  
}