export interface Filter {
  validated: boolean;
  vlast: string[];
  type: string[];

  // page
  start: number;
  n: number;

  // date
  dateStart: string;
  dateEnd: string;

  // area
  kraj: string[];
  okres: string[];
  orp: string[];
  mzchu: string[];
  vzchu: string[];

  // distance
  center: [number, number];
  radius: number;
}

export interface Page {
  pageIndex: number;
  pageSize: number;
  length: number;
}

export interface Kraj {
  ID: string;
  Name: string;
}

export interface Okres {
  ID: string;
  IDKraj: string;
  Name: string;
}

export interface Zchu {
  ID: string;
  IDKraj: string;
  IDOkres: string;
  Name: string;

}

export interface Orp {
  ID: string;
  Kategorie: string;
  Name: string;
}
