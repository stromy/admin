export const environment = {
  production: true,
  // server: 'http://localhost:3000',
  server: 'http://api.profesorbayer.cz',
  mapbox: {
    accessToken: 'pk.eyJ1IjoibW9qYWN6IiwiYSI6ImNraGh0Z2d5YzB2YTkyeXFxN3lidmtjMjYifQ.bKOUjMmSIav7UWJh1bFrwA'
  }
};
